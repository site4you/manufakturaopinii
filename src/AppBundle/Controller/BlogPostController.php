<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BlogPostController extends Controller
{
    /**
     * @Route("/blog", name="blogindexpost")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:BlogPost:index.html.twig', array(
        ));
    }

    /**
     * @Route("/private", name="blogprivatepost")
     */
    public function privateAction()
    {
        return $this->render('AppBundle:BlogPost:private.html.twig', array(
        ));
    }

}
