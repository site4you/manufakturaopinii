/**
 * Created by tomek on 22.01.2017.
 */
$(function () {
    // Smooth Scroll
    $('a[href*=#]').bind('click', function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });
});

$(document).ready(function() {
    var marginTop = null;
    var countDot = 0;
    $( ".timeline-item" ).each(function( index ) {
        if(marginTop !== null){
            $( this ).css('margin-top', marginTop + 'px');
        }
        if(countDot < 2){
            $(this).find('.timeline-badge a').css('background-color','#64B9C9');
            $(this).find('.timeline-badge .face, .timeline-badge .time').css('color','#64B9C9');
        }
        countDot++;
        marginTop = $( this ).height();
    });
})
